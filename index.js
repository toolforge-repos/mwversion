const { send } = require( 'micro' );
const fetch = require( 'node-fetch' );
const $ = require( 'cheerio' );

const releases = [
	'legacylts',
	'legacy',
	'lts',
	'stable',
	'beta',
	'alpha'
];

const items = [
	'branch',
	'version',
	'git',
	'date'
];

async function fetchVersion( release, item ) {
	const url = new URL( 'https://www.mediawiki.org/w/api.php' );
	url.searchParams.set( 'action', 'parse' );
	url.searchParams.set( 'format', 'json' );
	url.searchParams.set( 'formatversion', '2' );
	url.searchParams.set( 'prop', 'text' );
	url.searchParams.set( 'contentmodel', 'wikitext' );
	url.searchParams.set( 'disablelimitreport', '1' );
	url.searchParams.set( 'wrapoutputclass', '');
	url.searchParams.set( 'text', `{{#invoke:Version|get|${release}|${item}}}` );

	const result = await fetch( url, {
		headers: {
			'User-Agent': 'mwversion (https://tools.wmflabs.org/mwversion/)'
		}
	} );
	const data = await result.json();

	return {
		release,
		item,
		data: $( data.parse.text ).text().trim()
	};
}

async function fetchRelease( release ) {
	let requests = [];
	for ( const item of items ) {
		requests = [
			...requests,
			fetchVersion( release, item )
		];
	}

	return ( await Promise.all( requests ) ).reduce( ( data, version ) => {
		return {
			...data,
			[ version.item ]: version.data
		};
	}, {
		release
	} );
}

async function fetchData() {
	const items = [
		'branch',
		'version',
		'git',
		'date'
	];

	let requests = [];
	for ( const release of releases ) {
		for ( const item of items ) {
			requests = [
				...requests,
				fetchVersion( release, item )
			];
		}
	}

	const data = ( await Promise.all( requests ) ).reduce( ( map, version ) => {
		const existing = map.get( version.release ) || {
			release: version.release
		};

		map.set( version.release, {
			...existing,
			[ version.item ]: version.data
		} );

		return map;
	}, new Map() );

	return [ ...data.values() ].sort( ( a, b ) => {
		if ( a.branch < b.branch ) {
			return -1;
		}
		if ( a.branch > b.branch ) {
			return 1;
		}

		return 0;
	} );
}

async function handleRequest( req, res ) {
	res.setHeader( 'Access-Control-Allow-Origin', '*' );

	const url = new URL( req.url, 'http://localhost/' );

	const pathname = url.pathname.replace( /\/mwversion/g, '' );

	if ( pathname === '/' || pathname === '' ) {
		return send( res, 200, await fetchData() );
	}

	const release = pathname.substring( 1 );

	if ( releases.includes( release ) ) {
		return send( res, 200, await fetchRelease( release ) );
	}

	return send( res, 404, {
		error: 404
	} );
}

module.exports = handleRequest;
